def sumar(a,b):
    return a + b
def restar(a, b):
    return a - b

def programa_principal():
    print("La suma de 1 y 2 es", sumar(1, 2))
    print(f"La suma de 3 y 4 es {sumar(3,4)}")
    print(f"La resta de 5 y 6 es {restar(5,6)}")
    print(f"La resta de 7 y 8 es {restar(7,8)}")

programa_principal()

